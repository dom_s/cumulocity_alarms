angular.module('AlarmService', [])
    .service("alarm", ['$http', function($http) {
            this.all = [];

            this.getAllAlarms = function (callback) {
                var request = {
                    method: 'GET',
                    url: 'http://demos.cumulocity.com/alarm/alarms?pageSize=2000&resolved=false',
                    headers: {
                        'Authorization': 'Basic ZWthdGVyaW5hOkthdGUwMTIzNA==',
                        'Content-Type': 'application/vnd.com.nsn.cumulocity.alarmCollection+json;'
                    }
                };

                $http(request).then(function(result) {
                    return callback(result.data.alarms);
                }, function(error) {
                    return callback(error);
                })

            }
    }]);