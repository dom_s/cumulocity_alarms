'use strict';

var app = angular.module('myApp.alarmView', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/views', {
    templateUrl: 'views/alarmView.html',
    controller: 'alarmController'
  });
}]);

app.controller('alarmController', ['$scope', 'alarm', function($scope, alarm) {

  this.$onInit = function() {
    $scope.allAlarms = [];
    $scope.sortTypeSeverity = 'severity';
    $scope.sortTypeTimestamp = 'creationTime';

    alarm.getAllAlarms(function(result) {
      if(result)
        $scope.allAlarms = result;
      else
        alert(result);
    })
  };

}]);